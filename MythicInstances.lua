MythicInstances = {}

SLASH_MYTHIC1 = "/mythic"

local instances = {
	'Assault on Violet Hold',
	'Black Rook Hold',
	'Court of Stars',
	'Darkheart Thicket',
	'Eye of Azshara',
	'Halls of Valor',
	'Maw of Souls',
	'Neltharion\'s Lair',
	'The Arcway',
	'Vault of the Wardens',
}

function MythicInstances:GetList()
	nb_instances = GetNumSavedInstances()
	saved_instances = {}
	for i = 1, nb_instances, 1 do
		name, _,  _, _, locked, _, _, _, _, difficulty = GetSavedInstanceInfo(i)
		if difficulty == "Mythic" then
			saved_instances[name] = locked
		end
	end
	for _, v in ipairs(instances) do
		color = "cc0000"
		if saved_instances[v] == nil or not saved_instances[v] then
			color = "00cc00"
		end
		print("|cff" .. color .. " " .. v)
	end
end

SlashCmdList["MYTHIC"] = function(...)
	MythicInstances:GetList()
end
